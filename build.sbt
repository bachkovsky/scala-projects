name := "scala-tests"

version := "1.0"

scalaVersion := "2.12.1"
scalacOptions += "-Ypartial-unification"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"
libraryDependencies += "org.typelevel" %% "cats-core" % "1.3.1"
libraryDependencies += "org.typelevel" %% "cats-effect" % "1.0.0"
// https://mvnrepository.com/artifact/net.sf.sociaal/freetts
libraryDependencies += "net.sf.sociaal" % "freetts" % "1.2.2"
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.6"
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.6",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.6" % Test
)
// https://mvnrepository.com/artifact/com.google.code.gson/gson
libraryDependencies += "com.google.code.gson" % "gson" % "2.6.2"
libraryDependencies += "com.lihaoyi" %% "scalatags" % "0.6.7"
libraryDependencies += "com.github.pathikrit" % "better-files_2.12" % "3.5.0"
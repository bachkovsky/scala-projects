package func

object ValueClass extends App {

  case class Logarithm(d: Double) extends AnyVal

  object Logarithm {
    def safe(d: Double): Option[Logarithm] =
      if (d > 0.0) Some(Logarithm(math.log(d))) else None

    def exponent(l: Logarithm): Double = l.d
  }

  implicit class LogOps(val x: Logarithm) {
    def +(y: Logarithm): Logarithm =
      Logarithm(math.exp(x.d) + math.exp(y.d))

    def *(y: Logarithm): Logarithm =
      Logarithm(x.d + y.d)
  }

  //ok
  val l = Logarithm(1.0)
  val l2 = l * l
  val l3 = l + l
  println(l2)
  println(l3)

  //errors
  //val d: Double = l
  //val l4: Logarithm = 2.0
  //val l5 = l * 5

}

package func.typeclass.monads.io
import cats.effect.IO
import cats.implicits._

object IOTest extends App {

  def foo(): Unit = {
    println ("unpure")
  }

  def fooIO: IO [Unit] = {
    IO(print("pure"))
  }

  override def main(args: Array[String]): Unit = {
    val sequence = List(fooIO, fooIO, fooIO).sequence_
    sequence.unsafeRunSync()
  }
}

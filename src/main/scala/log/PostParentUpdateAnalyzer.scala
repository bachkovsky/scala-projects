package log

import java.nio.charset.StandardCharsets

import better.files._

import scala.util.matching.Regex

object PostParentUpdateAnalyzer extends App {
  val PostUpdate: Regex =
    """.*#.*\s+(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\.\d{2,3}).*postparentid=(\d+).* postid=(\d+).*""".r
  val file = File("/home/mike/hdd/logs/tat/PORTAL-44241/data1.txt")
  file.lines(StandardCharsets.UTF_8).foreach {
    case PostUpdate(time, parentId, postId) =>
      println(s"$time, postId=$postId, parentId=$parentId")
  }
}
